import React from 'react';
import { Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthLoadingScreen from './App/screens/AuthLoadingScreen';
import Login from './App/screens/MyAccount/Login';
import Register from './App/screens/MyAccount/Register';
import StartSession from './App/screens/StartSession/StartSession';
import SessionPreview from './App/screens/StartSession/SessionPreview';
import SessionInfo from './App/screens/StartSession/SessionInfo';
import SessionWarmup from './App/screens/StartSession/SessionWarmup';
import Menu from './App/screens/Store/Menu';
import Article from './App/screens/Store/Article';
import ArticlesByCategory from './App/screens/Store/ArticlesByCategory';
import MyInformation from './App/screens/MyInformation';
import ProfileUpdate from './App/screens/ProfileUpdate/ProfileInfo';
import Config from './App/screens/Config';
import { enableScreens } from 'react-native-screens';
import { header } from './App/styles/CommonStyle'
import SessionView from './App/screens/SessionView/SessionView';
import SessionSettings from './App/screens/SessionView/SessionSettings';
import QuestionScreen from './App/screens/QuestionScreen';


export default function App() {

  enableScreens();

  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="AuthLoading">
        <Stack.Screen
          name="AuthLoading"
          component={AuthLoadingScreen}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: 'Registrate',
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />
          }}
        />
        <Stack.Screen
          name="StartSession"
          component={StartSession}
          options={{
            title: '',
            headerStyle: header.headerStyle
          }}
        />
        <Stack.Screen
          name="SessionPreview"
          component={SessionPreview}
          options={{
            title: '',
            headerStyle: header.headerStyle,
          }}
        />
        <Stack.Screen
          name="SessionInfo"
          component={SessionInfo}
          options={{
            title: '',
            headerStyle: header.headerStyle,
          }}
        />
        <Stack.Screen
          name="SessionView"
          component={SessionView}
          options={{
            title: '',
            headerStyle: header.headerStyle,
          }}
        />
        <Stack.Screen
          name="SessionWarmup"
          component={SessionWarmup}
          options={{
            title: '',
            headerStyle: header.headerStyle,
          }}
        />
        <Stack.Screen
          name="Menu"
          component={Menu}
          options={{
            title: '',
            headerStyle: header.headerStyle,
            headerTitleAlign: 'center',
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
        <Stack.Screen
          name="SessionSettings"
          component={SessionSettings}
          options={{
            title: '',
            headerStyle: header.headerStyle,
            headerTitleAlign: 'center',
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
        <Stack.Screen
          name="Article"
          component={Article}
          options={{
            title: '',
            headerStyle: header.headerStyle,
            headerTitleAlign: 'center',
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
        <Stack.Screen
          name="ArticlesByCategory"
          component={ArticlesByCategory}
          options={{
            title: '',
            headerStyle: header.headerStyle,
            headerTitleAlign: 'center',
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
        <Stack.Screen
          name="Config"
          component={Config}
          options={{
            title: 'Configuración',
            headerStyle: header.headerStyle,
            headerTitleAlign: 'center',
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
        <Stack.Screen
          name="MyInformation"
          component={MyInformation}
          options={{
            title: 'Mi información',
            headerTitleAlign: 'center',
            headerStyle: header.headerStyle,
            headerTitleStyle: header.headerTitleStyle,
          }}
        />
        <Stack.Screen
          name="QuestionScreen"
          component={QuestionScreen}
          options={{
            title: 'Cuestionario',
            headerTitleAlign: 'center',
            headerStyle: header.headerStyle,
            headerTitleStyle: header.headerTitleStyle,
          }}
        />
        <Stack.Screen
          name="ProfileUpdate"
          component={ProfileUpdate}
          options={{
            title: 'Actualizar información',
            headerTitleAlign: 'center',
            headerStyle: header.headerStyle,
            headerTitleStyle: header.headerTitleStyle,
            headerBackImage: () => <Image style={{
              width: 35, height: undefined, aspectRatio: 1,
            }} source={require('./assets/back.png')} />,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
