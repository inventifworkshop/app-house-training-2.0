import React from 'react';
import t from 'tcomb-form-native';
import formValidation from '../utils/validation';
import inputTemplate from './templates/Input';
import textInputTemplate from './templates/TextInput';
import select from './templates/Select';
import agePicker from '../utils/User';
export const RegisterStructure = t.struct({
  name: t.String,
  lastName: t.String,
  secondLastName: t.String,
  age: t.Number,
  address: t.String,
  phone: formValidation.phone,
  email: formValidation.email,
  password: formValidation.password,
  passwordConfirmation: formValidation.password,
});
export const RegisterOptions = {
  fields: {
    /*  age:{
  
          config:{
            placeholder: 'Selecciona tu edad',
            label:'* edad:',
            error:'',
            iconName:'user'
          }
      },*/
    name: {
      template: textInputTemplate,
      config: {
        label: 'Nombre:',
        placeholder: 'Escribe tu nombre ',
        error: 'El nombre es requerido',
        iconName: 'user'
      }
    },
    lastName: {
      template: textInputTemplate,
      config: {
        label: 'Primer Apellido:',
        placeholder: 'Escribe tu primer apellido',
        error: 'El primer apellido es requerido',
        iconName: 'user'
      }
    },
    secondLastName: {
      template: textInputTemplate,
      config: {
        label: 'Segundo Apellido:',
        placeholder: 'Escribe tu segundo apellido',
        error: 'El nombre es requerido',
        iconName: 'user'
      }
    },
    age: {
      template: textInputTemplate,
      config: {
        label: 'Edad:',
        placeholder: 'Escribe tu edad',
        error: 'La edad es requeridad',
        keyboardType:'numeric',
        maxLength: 2
      }
    },
    phone: {
      template: textInputTemplate,
      config: {
        label: "Teléfono",
        placeholder: "(686)-123-4567",
        error: 'Teléfono invalido',
        iconName: 'phone',
        keyboardType:'numeric',
        maxLength: 10
      }
    },
    address: {
      template: textInputTemplate,
      config: {
        label: "Dirección",
        placeholder: "Escribe tu dirección",
        error: 'Teléfono invalido',
        iconName: 'phone'
      }
    },
    email: {
      template: textInputTemplate,
      config: {
        label: 'Correo',
        placeholder: 'tucorreo@correo.com',
        error: 'Correo invalido',
        iconName: 'at'
      }
    },
    password: {
      template: textInputTemplate,
      config: {
        label: 'Contraseña',
        placeholder: 'Escribe tu contraseña',
        error: 'Contraseña invalida',
        password: true,
        secureTextEntry: true,
        iconName: 'unlock'
      }
    },
    passwordConfirmation: {
      template: textInputTemplate,
      config: {
        label: 'Repite la contraseña',
        placeholder: 'Repite la contraseña',
        error: 'Las contraseñas no son iguales',
        password: true,
        secureTextEntry: true,
        iconName: 'lock'
      }
    }
  }
};
