import React from 'react';
import {StyleSheet,View} from 'react-native';
import {Input,Icon} from 'react-native-elements';
export default (selecTemplate = locals=>{
  return (
    <View style={styles.viewContainer}>
  <Input
  placeholder={locals.config.placeholder}
  password= {locals.config.password}
  secureTextEntry= {locals.config.secureTextEntry}
  rightIcon={<Icon
    name={locals.config.iconName}
    type='font-awesome'
    size={24}
    color='black'

    />}
    onChangeText = {value => locals.onChange(value)}
  />
    </View>
  );
});

const styles = StyleSheet.create({
viewContainer:{
  marginTop:12,
  marginBottom:12
}
});
