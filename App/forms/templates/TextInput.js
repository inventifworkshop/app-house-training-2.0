import React from 'react';
import { StyleSheet, View, TextInput, Text } from 'react-native';

export default (inputTemplate = locals => {
    return (
        <View style={styles.viewContainer}>

            <Text style={styles.text}>{locals.config.label}</Text>
            <TextInput
                style={styles.input}
                placeholder={locals.config.placeholder}
                password={locals.config.password}
                secureTextEntry={locals.config.secureTextEntry}
                onChangeText={value => locals.onChange(value)}
                keyboardType={locals.config.keyboardType}
                maxLength={locals.config.maxLength}
            />

        </View>
    );
});

const styles = StyleSheet.create({
    viewContainer: {
        marginTop: 10,
        marginBottom: 10
    },
    input: {
        borderWidth: 2,
        borderRadius: 4,
        borderColor: "#EEEEEE",
        height: 35,
        paddingLeft: 10,
        paddingRight: 10,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16
    }
});
