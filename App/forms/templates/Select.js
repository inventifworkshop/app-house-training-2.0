import React from 'react';
import {StyleSheet,View,Picker} from 'react-native';
import {Input,Text} from 'react-native-elements'

export default (Select = locals=>{
  return (
    <View style={styles.viewContainer}>
    <Text style={styles.selectText}>{locals.config.placeholder}</Text>
    <Picker
  selectedValue={'java'}
  style={{height: 20, width: 100,fontSize:24}}
  onValueChange={(itemValue, itemIndex) =>{
    {value => locals.onChange(value)}
  }
    //this.setState({language: itemValue})
  }>
  <Picker.Item label="18" value="18" />
  <Picker.Item label="19" value="19" />
 <Picker.Item label="20" value="20" />
  <Picker.Item label="21" value="21" />
  <Picker.Item label="22" value="22" />
  <Picker.Item label="23" value="23" />

</Picker>
</View>
)});
const styles = StyleSheet.create({
viewContainer:{
  marginTop:12,
  marginBottom:12,
  flexDirection: 'row',
  justifyContent: 'center'

},
selectText:{
  fontSize:20
}
});
