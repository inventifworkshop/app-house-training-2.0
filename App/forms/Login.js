import React from 'react';
import t from 'tcomb-form-native';
import formValidation from '../utils/validation';
import inputTemplate  from './templates/Input';
export const LoginStructure = t.struct({
  email: formValidation.email,
  password: formValidation.password,
});
export const LoginOptions ={
  fields:{
    email:{
      template:inputTemplate,
      config:{
        label: '* Correo',
        placeholder: 'tucorreo@correo.com',
        error: 'Correo invalido',
      }
    },
    password:{
      template:inputTemplate,
      config:{
        label:'* Contraseña',
        placeholder: 'Escribe tu contraseña',
        error: 'Contraseña invalida',
        password:true,
        secureTextEntry:true,
      }
    },
  }

}
