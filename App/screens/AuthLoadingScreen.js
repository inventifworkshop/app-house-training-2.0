import React, { Component } from 'react';
import { CommonActions } from '@react-navigation/native';
import {
  ActivityIndicator,
  AsyncStorage,
  View
} from 'react-native';

export default class AuthLoadingScreen extends Component {

  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    console.log('userToken(Auth): ', userToken);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    const route = userToken ? 'StartSession' : 'Login';

    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          { name: route }
        ]
      })
    );
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}