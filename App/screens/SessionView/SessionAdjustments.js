import React, { Component } from 'react';
import { View, Text } from 'react-native'
import Timer from './Timer';
import { CustomButton } from '../../Components/Common/CustomButton';
import {buttons} from '../../styles/CommonStyle';
import SessionSettings from '../SessionView/SessionSettings';

export default class SessionAdjustments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inAdjustments:false
    }
    }

    goToAdjustments =()=>{
      this.setState({inAdjustments:true});
    }

    backFromAdjustments=(newTimes)=>{
      this.props.onTimesUpdate(newTimes)
      this.setState({inAdjustments:false});
      this.props.onFinish();
    }
    
    timeUp = ()=>{
      if(!this.state.inAdjustments){
        this.props.onFinish();
      }
    }

    render = () => {
        return ( 
          this.state.inAdjustments? 

          <SessionSettings defaultWorkTime={this.props.defaultWorkTime} exercise={this.props.exercise} onExerciseTimeUpdate={this.props.onExerciseTimeUpdate} restTime={this.props.restTime} roundRestTime={this.props.roundRestTime} onAdjust={this.backFromAdjustments}/>:
            <View>
                <View>
                    <Timer
                      timer={this.props.restTime}
                      onFinish={this.timeUp}
                      state="start"
                    />
                </View>
                <View>
                  <Text>
                    Desea ajustar los tiempos e intervalos?
                  </Text>
                </View>
                <View>
                    <Text>
                      Tiempo de descanso de ejercicio: {this.props.restExercise}
                    </Text>
                </View>
                <View>
                    <Text>
                      Tiempo de descanso de round {this.props.restRound}
                    </Text>     
                </View>
                <View>
                    <Text>
                      <CustomButton title="Ajustar tiempos" style={buttons.primaryButton}
                      onPress={this.goToAdjustments}/>
                    </Text>     
                </View>
            </View>

        )
    }
}