import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import Loader from '../../Components/Common/Loader';
import { getTodaySessions } from '../../utils/API';
import { containers, images, texts, buttons, header } from '../../styles/CommonStyle';
import { CustomButton } from '../../Components/Common/CustomButton';
import Timer from './Timer';
import SessionAdjustments from './SessionAdjustments';

export default class SessionView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      exerciseIndex: 0,
      currentExercise: {},
      timer: 0,
      exercises: [],
      state: "stop",
      defaultTime: 0,
      rounds: 0,
      currentRound: 0,
      restTime: 0,
      roundRestTime: 0,
    }
  }

  interval = {};

  async componentDidMount() {
    let exercises = [];
    let sessionData = {};
    const { params } = this.props.route;
    sessionData = params ? params.sessionData : null;
    exercises = sessionData.exercises;
    let defaultTime = 3000//parseInt(sessionData.work) * 1000 || 30000;

    let current = exercises[this.state.exerciseIndex];
    
    if (current) {
      this.setState({ exercises: exercises, restTime: sessionData.rest_exercise || 20000, roundRestTime: sessionData.rest_rounds, currentExercise: current, defaultTime: defaultTime, timer: sessionData.work || defaultTime });
    }

    this.setHeader();

  }

  setHeader = () => {
    this.props.navigation.setOptions({
      headerLeft: () =>
        <View style={header.headerLeft}>
          <TouchableOpacity onPress={this.goToConfiguration}>
            <Image source={require('../../../assets/config.png')}
              style={header.headerButtons}
            />
          </TouchableOpacity>
        </View>
      ,
      headerRight: () =>
        <View style={header.headerRight}>
          <TouchableOpacity onPress={this.goBack}>
            <Image source={require('../../../assets/back.png')}
              style={header.headerButtons}
            />
          </TouchableOpacity>
        </View>
    });

  }

  goToConfiguration = () => {
    this.props.navigation.navigate("Config");
  }

  goBack = () => {
    this.props.navigation.goBack();
  }

  goToIndex = () => {
    this.props.navigation.navigate("StartSession");
  }

  getNextExercise = () => {
    if (this.state.state === "running") {
      let currentIndex = this.state.exerciseIndex + 1;
      let current = this.state.exercises[currentIndex];
      if (current) {
        this.setState({ state: "resting", currentExercise: current, exerciseIndex: currentIndex })
      }
      else {
        this.setState({ state: "finished" })
      }
    }
    else if (this.state.state === "resting") {
      if (this.state.currentExercise) {
        this.setState(previousState => ({ state: "start" }));
      }
      else {
        this.setState({ state: "finished" })
      }
    }

  }

  onStartTimer = () => {
    this.setState({ state: "running" });
  }

  startRoutine = () => {
    if (this.state.state !== "finished" && this.state.state !== "started") {
      this.setState({ state: "start" });
    }
    if(this.state.state === "running"){
      this.setState({ state: "pause" });
    }
    if(this.state.state === "pause"){
      this.setState({ state: "start" });
    }
  }

  updateTimes = (newTimes) => {
    this.setState({
      restTime: newTimes.restTime,
      roundRestTimes: newTimes.roundRstTime
    });
  }

  udpateExerciseTime = (exerciseId, newExerciseTime)=>{
    let allExercises=[...this.state.exercises]
    allExercises.forEach(exercise=>{
      if(exerciseId === exercise.id ){
        exercise.workTime = newExerciseTime;
      }
    });

    let currentExercise = {...this.state.currentExercise,
    workTime: newExerciseTime};
    console.log("All exercises ",allExercises);
    this.setState({exercises:allExercises, currentExercise:currentExercise});
  }

  render() {
    return (
      <SafeAreaView style={[containers.mainContainer,{flexDirection:"column"}]}>

        <View style={[containers.headerContainer, { flex:12  }]}>
          <Image source={require("../../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '50%' }]} />
        </View>

        {this.state.state === "finished" ?

          <View style={[containers.mainContainer, { flex:88 }]} >

            <View style={{ marginTop: 30, height: '15%' }}>
              <Text style={texts.bigText}>Felicidades!</Text>
            </View>

            <View style={[containers.imageContainer, { marginVertical: 20 }]}>
              <Image source={require("../../../assets/icons/finish.png")} resizeMode="contain" />
            </View>

            <View style={{ marginTop: 30, height: '20%' }}>
              <CustomButton
                title="Terminar"
                onPress={this.goToIndex}
                style={buttons.primaryButton}
                textStyle={texts.primaryText}
              />
            </View>
          </View>
          :
          this.state.state == "resting" ?
            <SessionAdjustments
              restTime={this.state.restTime}
              roundRestTime={this.state.roundRestTime}
              onTimesUpdate={this.updateTimes}
              onFinish={this.getNextExercise}
              onExerciseTimeUpdate={this.udpateExerciseTime}
              navigation={this.props.navigation}
              exercise={this.state.currentExercise}
              defaultWorkTime={this.state.timer}
            ></SessionAdjustments>
            :
            <View style={[containers.mainContainer, { flex:88 }]} >

              <Timer
                state={this.state.state}
                onFinish={this.getNextExercise}
                onStart={this.onStartTimer}
                timer={this.state.currentExercise.workTime || this.state.timer}
              />

              <View style={[containers.imageContainer, { height: '44%' }]}>
                <Image resizeMode="contain" style={images.image} source={this.state.currentExercise.image ? { uri: this.state.currentExercise.image.replace(";", "") } : require("../../../assets/Splash_2.png")} />
              </View>

              <View style={styles.infoContainer}>
                <Text style={texts.textName}>{this.state.currentExercise.name}</Text>
                <Text style={texts.textDescription}>{this.state.currentExercise.description}</Text>
              </View>

              <View style={{ height: '4%' }}>
                <CustomButton
                  title={this.state.state === "stop" ? "Adelante!" : 
                  this.state.state==="pause"?"Continuar!":"En marcha"}
                  onPress={this.startRoutine}
                  style={[buttons.primaryButton, { alignSelf: 'center' }]}
                  textStyle={texts.primaryText} 
                />
              </View>

            </View>
        }

      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  viewContainer: {
    width: '50%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    width: '99%',
    height: '99%',
    justifyContent: 'center',
    alignItems: 'center',

  },
  infoContainer: {
    height: '15%',
    marginVertical: 15,
    marginHorizontal: 15,
  },
})

