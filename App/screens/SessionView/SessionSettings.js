import React, { Component } from 'react';
import { View, Text } from 'react-native'
import Timer from './Timer';
import { CustomButton } from '../../Components/Common/CustomButton';
import {button, buttons ,inputs} from '../../styles/CommonStyle';
import NumberSelection from '../../Components/Common/NumberSelector';

export default class SessionAdjustments extends Component {
    constructor(props) {
        super(props);
        this.state = {
          inAdjustments:false,
          restTime:"0",
          cycleRestTime:"0",
          exerciseTime: props.exercise.workTime && props.exercise.workTime.toString() || props.defaultWorkTime,
          restTime: props.restTime && props.restTime.toString() || "0",
          roundRestTime: props.roundRestTime && props.roundRestTime.toString() || "0",
          nextExercise: {
            
          }
        }
    }

    componentDidUpdate= (prevProps) =>{
      if(this.props.restTime!=prevProps.restTime || this.props.roundRestTime!=prevProps.roundRestTime){
        this.setState({
          restTime: this.props.restTime,
          roundRestTime: this.props.roundRestTime
        });
      }
    }

    validateNonEmpty = (value) =>{
      if(value.length>0){
        if(value.length>1 && value.charAt(0)=="0"){
          value = value.substring(1);
        }
        return value;
      }
      else{
        return "0";
      }
    }

    onRestTimeChange = (newValue)=>{
      this.setState({restTime:this.validateNonEmpty(newValue.toString())});
    }


    onRoundRestTimeChange = (newValue)=>{
      this.setState({roundRestTime:this.validateNonEmpty(newValue.toString())});
    }

    onExerciseTimeChange = (newValue)=>{
      this.setState({exerciseTime:this.validateNonEmpty(newValue.toString())});
    }

    goToAdjustments =()=>{
      this.setState({inAdjustments:true});
      this.props.navigation.navigate("SessionSettings",{});
    }

    timeUp = ()=>{
      if(!this.state.inAdjustments){
        this.props.onFinish
      }
    }

    

    changeExerciseSettings=()=>{
      //Request change exercise times
      if(!this.props.exercise.workTime && parseInt(this.state.exerciseTime)!=0 ||
        this.props.exercise.workTime != parseInt(this.state.exerciseTime)){
          this.props.onExerciseTimeUpdate(this.props.exercise.id, parseInt(this.state.exerciseTime));
        }
      this.props.onAdjust({restTime:this.state.restTime, roundRestTime:this.state.roundRestTime});
    }

    render = () => {
        return ( 
            <View style={{flex:88,flexDirection:"column"}}>
                <View style={{flex:1}}>
                  <Text>Session time adjustments</Text>
                </View>
                <View style={{flex:1}}>
                  <Text>
                    Session time settings
                  </Text>
                </View>
                <View style={{flex:1}}>
                    <Text>
                      Tiempo de descanso de ejercicio :
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text>
                      (Actual {this.state.restTime})
                    </Text>
                </View>
                <View style={{flex:1}}>
                <NumberSelection
                    default={this.state.restTime}
                    onChange={this.onRestTimeChange}
                  />
                </View>
                <View style={{flex:1}}>
                    <Text >
                      Tiempo de descanso de round 
                    </Text>     
                </View>
                <View style={{flex:1}}>
                    <Text >
                      (Actual: {this.state.roundRestTime})
                    </Text>     
                </View>
                <View style={{flex:1}}>
                <NumberSelection
                    default={this.state.roundRestTime}
                    onChange={this.onRoundRestTimeChange}
                  />
                </View>

                <View style={{flex:1}}>
                    <Text >
                      Siguiente ejercicio {this.props.exercise.name}
                    </Text>     
                </View>
                <View style={{flex:1}}>
                    <Text >
                      Tiempo: { this.state.exerciseTime.toString() })
                    </Text>     
                </View>
                <View style={{flex:1}}>
                  <NumberSelection
                    default={this.props.exercise.workTime || this.state.exerciseTime}
                    onChange={this.onExerciseTimeChange}
                  />
                </View>

                <View style={{flex:6, alignSelf:"center", justifyContent:"center"}}>
                    <CustomButton style={buttons.primaryButton} title="Aceptar" onPress={this.changeExerciseSettings}/>
                </View>
                
            </View>

        )
    }
}