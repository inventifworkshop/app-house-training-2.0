import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import Loader from '../../Components/Common/Loader';
import { getTodaySessions } from '../../utils/API';
import {containers, images, texts, buttons} from '../../styles/CommonStyle';
import { Audio } from 'expo-av';
import { CustomButton } from '../../Components/Common/CustomButton';

export default class Timer extends Component {
    constructor(props){
        super(props);
        this.state={
        timer: 0,
        status:"stopped"
    }
    }
    

    componentDidMount=()=>{
        if(!this.props.state || this.props.state==="start"){
            this.setState({timer:this.props.timer*1000, status :"running"});
            this.startTimer();
        }
    }



    componentDidUpdate = (previousProps) => {
        if(this.state.status === "stopped" && this.props.state === "start" && !this.interval){
            this.setState({timer:this.props.timer*1000, status :"running"});
            this.startTimer();
        }
        if(this.state.status === "pause"  && this.props.state === "start" ){
          this.setState({status :"running"});
          this.startTimer();
        }

        if(this.state.status === "running" && this.props.state === "pause"){
          this.pause();
      }
    }
    interval = null;

    pause = () =>{
      this.setState({status :"pause"});
      if(this.interval){
        clearInterval(this.interval);
      }
    }

    finish = () =>{
        this.setState({status:"stopped"});
        this.props.onFinish();
    }
    startTimer() {
        let newTime = 0;
        if(this.interval){
          clearInterval(this.interval);
        }
        
        this.interval = setInterval(async () => {
          newTime = this.state.timer - 1000;
          if (newTime <= 0) {
            clearInterval(this.interval);
            newTime = 0;
            this.setState(previousState => ({ timer: newTime }));
            this.playSound();
            this.finish();
          }
          else {
            this.setState(previousState => ({ timer: newTime }));
          }
    
        }, 1000);
        if(this.props.onStart){ 
            this.props.onStart();
        }
      }

    playSound = async () => {
        const soundObject = new Audio.Sound();
        try {
          await soundObject.loadAsync(require('../../../assets/audio/success.wav'));
          await soundObject.playAsync();
          // Your sound is playing!
        } catch (error) {
          // An error occurred!
        }
    }

    render = () => {
        return (
            <View style={styles.viewContainer}>
              <ImageBackground source={require("../../../assets/timer2.png")} resizeMode='cover' style={styles.backgroundImage}>
                <Text style={texts.bigText}>{(this.state.timer / 1000)}</Text>
              </ImageBackground>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    viewContainer: {
      width: '50%',
      height: '25%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    backgroundImage: {
      width: '99%',
      height: '99%',
      justifyContent: 'center',
      alignItems: 'center',
  
    },
    infoContainer: {
      height: '15%',
      marginVertical: 15,
      marginHorizontal: 15,
    },
  })