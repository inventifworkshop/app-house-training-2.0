import React, { Component } from 'react';
import { View, StyleSheet, Text, Alert, AsyncStorage } from 'react-native';
import { Image, Button, Divider } from 'react-native-elements';
import { CommonActions } from '@react-navigation/native';
import Toast from 'react-native-easy-toast';
import t from 'tcomb-form-native';
const Form = t.form.Form;
import { LoginStructure, LoginOptions } from '../../forms/Login';
import { images } from '../../styles/CommonStyle';
import { manualLogin } from '../../utils/API';
import { Validations } from '../../Helpers';
import localAPI from '../../utils/LocalAPI';

export default class Login extends Component {

  constructor() {
    super();
    this.state = {
      loginStructure: LoginStructure,
      loginOptions: LoginOptions,
      loginData: {
        email: '',
        password: ''
      },
      loginErrorMessage: ''
    };;
  }

  formLoginOnChange = (formValue) => {
    this.setState({
      loginData: formValue
    })
  }

  loginRequest = async () => {
    let loggedIn = false;
    console.log("Ready to send ", this.state.loginData.email, " AND ", this.state.loginData.password);
    let loginResult = await manualLogin({ email: this.state.loginData.email, password: this.state.loginData.password })

    if (loginResult.status === "success" && loginResult.code === 200) {
      loggedIn = true;
    }
    if (loggedIn) {

      localAPI.saveUser(loginResult.user);

      const infoToken = await AsyncStorage.getItem('infoToken');
      console.log('infoToken (Login): ', infoToken);
      const route = infoToken ? 'StartSession' : 'MyInformation';

      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: route,
            }
          ]
        })
      );
    }
    else {
      Alert.alert("Error de inicio de sesión", "Hubo un problema al iniciar sesión. Asegúrese de ingresar correctamente sus credenciales. ")
    }

    return loggedIn;
  }

  login = async () => {
    let { email, password } = this.state.loginData;

    const response = Validations.loginValidation(email, password);
    if (!response.validate) {
      this.setState({ loginErrorMessage: response.errorMessage });
    } else {
      this.setState({
        loginErrorMessage: ''
      });
      await this.loginRequest();
    }
  }

  render() {

    const { loginStructure, loginOptions, loginErrorMessage } = this.state;

    return (
      <View style={styles.viewBody}>
        <Image
          source={require('../../../assets/logo.png')}
          style={images.logo}
          containerStyle={styles.containerLogo}
          resizeMode='contain'
        />
        <View style={styles.formContainer}>
          <Form
            style={styles.loginForm}
            ref='loginForm'
            type={loginStructure}
            options={loginOptions}
            value={this.state.loginData}
            onChange={formValue => this.formLoginOnChange(formValue)}
          />
          <Button buttonStyle={styles.buttonLogin} title='Iniciar sesión' onPress={() => this.login()} />
          <Text style={styles.textRegister}>¿Aun no tienes una cuenta?
            <Text style={styles.txtRegister} onPress={() => { this.props.navigation.navigate("Register"); }}> Regístrate</Text>
          </Text>
          <Text style={styles.loginErrorMessage}>{loginErrorMessage}</Text>
          <Divider style={styles.divider} />
        </View>

        <Toast
          ref='toastLogin'
          position='bottom'
          positionValue={450}
          fadeInDuration={1000}
          fadeOutDuratio={1000}
          opacity={0.8}
          textStyle={{ color: '#fff' }}
        />

      </View>

    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 20,
  },
  loginForm: {
    marginLeft: 40,
    marginRight: 40,
  },
  buttonLogin: {
    backgroundColor: '#FF2D59',
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
  },
  formContainer: {
    marginTop: 10,
  },
  containerLogo: {
    marginTop: 50,
    alignSelf: 'center',
  },
  loginErrorMessage: {
    color: 'red',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  divider: {
    backgroundColor: '#FF2D59',
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
    alignSelf: 'center'
  },
  txtRegister: {
    color: '#FF2D59',
    fontWeight: 'bold',
  }
});
