import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import t from 'tcomb-form-native';
const Form = t.form.Form;
import { RegisterStructure, RegisterOptions } from '../../forms/Register';
import Toast from 'react-native-easy-toast';
import { manualSignIn } from '../../utils/API';
import { Validations } from '../../Helpers';

export default class Register extends Component {

    constructor() {
        super();
        this.state = {
            registerStructure: RegisterStructure,
            registerOptions: RegisterOptions,
            formData: {
                name: '',
                lastName: '',
                secondLastName: '',
                email: '',
                password: '',
                passwordConfirmation: '',
                age: '',
                address: '',
                phone: '',
            },
            formErrorMessage: ''
        }
    }

    manualRegister = async () => {
        //This will store this.state.formData on formData variable
        let { formData } = this.state;
        let newUserData = {
            "name": formData.name,
            "last_name": formData.lastName,
            "second_name": formData.secondLastName,
            "phone": formData.phone,
            "address": formData.address,
            "email": formData.email,
            "password": formData.password,
            "auth_method": "manual"
        }

        return await manualSignIn(newUserData);
    }

    register = async () => {
        console.log(this.state.formData);

        const {
            name,
            lastName,
            secondLastName,
            age,
            address,
            phone,
            email,
            password,
            passwordConfirmation
        } = this.state.formData;

        let response = Validations.validatePassword(password, passwordConfirmation);

        if (name && lastName && secondLastName && age && address && phone && email && password && passwordConfirmation) {
            if (Validations.validateEmail(email)) {
                if (response.validate) {
                    this.setState({ formErrorMessage: '' });

                    let registerResult = await this.manualRegister();

                    if (registerResult && registerResult.code === 200 && registerResult.status === "success") {
                        Alert.alert("Registro exitoso", "Su cuenta ha sido creada, ahora puede iniciar sesión.", [
                            {
                                text: "Log in", onPress: () => {
                                    this.props.navigation.navigate("Login");
                                }
                            }
                        ]);
                    }
                    else {
                        if (registerResult.message && registerResult.message.email) {
                            Alert.alert("Email inválido", registerResult.message.email.toString());
                        }
                        else {
                            alert("Error al registrar la cuenta", "Hubo un problema con el registro. Vuelve a intentarlo más tarde.");
                        }
                    }
                } else {
                    console.log('')
                    this.setState({ formErrorMessage: response.errorMessage })
                }
            } else {
                this.setState({ formErrorMessage: 'El correo no es válido.' })
            }
        }
        else {
            this.setState({ formErrorMessage: 'Por favor, llene todos los campos.' });
        }
    }

    onChangeRegister = (formValue) => {
        this.setState({
            formData: formValue,
        });
    }

    render() {
        const { registerStructure, registerOptions, formErrorMessage } = this.state;

        return (
            <KeyboardAwareScrollView
                contentContainerStyle={{ backgroundColor: '#fff', flexGrow: 1, justifyContent: 'center' }}>
                <View style={styles.viewBody}>
                    <Form
                        ref='registerForm'
                        type={registerStructure}
                        options={registerOptions}
                        value={this.state.formData}
                        onChange={formValue => this.onChangeRegister(formValue)}
                    />
                    <Button buttonStyle={styles.buttonRegister} title='Completar Registro' onPress={(formValue) => { this.register(formValue) }} />
                    <View style={{ height: 100 }}>
                        <Text style={styles.formErrorMessage}>{formErrorMessage}</Text>
                    </View>
                    <Toast
                        ref='toast'
                        position='bottom'
                        positionValue={150}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        opacity={0.8}
                        textStyle={{ color: '#fff' }}
                    />
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({
    viewBody: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        marginLeft: 30,
        marginRight: 30,
    },
    buttonRegister: {
        backgroundColor: '#FF2D59',
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
    },
    formErrorMessage: {
        color: '#FF2D59',
        textAlign: 'center',
        marginTop: 30,
    }
});
