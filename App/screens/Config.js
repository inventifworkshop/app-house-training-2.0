import React, { Component } from 'react';
import { StyleSheet, View, Alert, AsyncStorage } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { buttons, containers } from '../styles/CommonStyle';
import { CustomButton } from '../Components/Common/CustomButton';
import { Utils } from '../Helpers';

export default class Config extends Component {

    clearToken = () => {

        //if (Utils.removeKeySession('userToken')) {
        if (AsyncStorage.clear()) {
            this.props.navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'Login',
                        }
                    ]
                })
            );
        }
    }

    goToMyInformation = () => {
        this.props.navigation.push("MyInformation");
    }

    goToProfileUpdate = () =>{
        this.props.navigation.push("ProfileUpdate");
    }
    
    goToQuestionScreen = () => {
        this.props.navigation.push("QuestionScreen");
    }

    logOut = () => {
        Alert.alert(
            'Confirmación requerida'
            , '¿Realmente quieres cerrar tu sesión?'
            , [
                { text: 'Si', onPress: this.clearToken },
                { text: 'No' }
            ]
        );
    }

    showAlert = () => {
        Alert.alert('Aviso', 'De momento seguimos trabajando en esta función');
    }

    render() {
        return (
            <View style={containers.mainContainer}>

                <View style={containers.hairline} />

                <CustomButton title="Actualizar perfil"
                    onPress={this.goToProfileUpdate}
                    style={buttons.styleButton}
                />

                <CustomButton title="Cerrar sesión"
                    onPress={this.logOut}
                    style={[buttons.styleButton, styles.lastButton]}
                />

                <View style={containers.hairline} />

                <CustomButton title="Notificaciones"
                    onPress={this.showAlert}
                    style={buttons.styleButton}
                />

                <CustomButton title="Sonidos"
                    onPress={this.showAlert}
                    style={buttons.styleButton}
                />

                <CustomButton title="Ayuda"
                    onPress={this.showAlert}
                    style={[buttons.styleButton, styles.lastButton]}
                />

                <View style={containers.hairline} />

                <CustomButton title="Pagos"
                    onPress={this.showAlert}
                    style={buttons.styleButton}
                />

                <CustomButton title="Obtener suscripción"
                    onPress={this.showAlert}
                    style={buttons.styleButton}
                />

                <CustomButton title="Mi información"
                    onPress={this.goToMyInformation}
                    style={[buttons.styleButton, styles.lastButton]}
                />

                <CustomButton title="Cuestionario"
                    onPress={this.goToQuestionScreen}
                    style={[buttons.styleButton, styles.lastButton]}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    lastButton: {
        borderBottomWidth: 0,
        marginHorizontal: 10
    },
})
