import React, { Component } from 'react';
import { AsyncStorage, View, Image, StyleSheet, Alert, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { TextInput, RadioButton, Text } from 'react-native-paper';
import { CommonActions } from '@react-navigation/native';
import { buttons, containers, header, images, texts } from '../styles/CommonStyle';
import { CustomButton } from '../Components/Common/CustomButton';
import { Utils } from '../Helpers';
import { updateUser } from '../utils/API';
import localAPI from '../utils/LocalAPI';

export default class myInformation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isInfoToken: false,
            userToken: ''
        }
    }

    async componentDidMount() {
        let user = await localAPI.getUser();
        this.setState({ user: user });
        console.log('User(myInformation): ', this.state.user);
        this.getToken();
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    omitir = () => {
        Alert.alert(
            '¿Omitir datos iniciales?'
            , 'Para sesiones más personalizadas es necesario que ingreses estos datos, igualmente podrás ingresarlos mas tarde.'
            , [
                { text: 'Si', onPress: this.goToStartSession },
                { text: 'No' }
            ]
        );
    }

    getToken = async () => {

        const userToken = await AsyncStorage.getItem('userToken');
        console.log('userToken(myInformation): ', userToken);
        this.setState({ userToken: userToken });

        const infoToken = await AsyncStorage.getItem('infoToken');
        console.log('infoToken(myInformation): ', infoToken);

        if (!infoToken) {
            Utils.createKeySession('infoToken');
            const infoToken = await AsyncStorage.getItem('userToken');
            console.log('infoToken created(MyInformation): ', infoToken);
        }

        this.setState({ isInfoToken: infoToken ? true : false })
        console.log('isInfoToken: ', this.state.isInfoToken);

        this.props.navigation.setOptions({
            headerLeft: () => this.state.userToken ? (
                <View style={header.headerLeft}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Image source={require('../../assets/back.png')}
                            style={header.headerButtons}
                        />
                    </TouchableOpacity>
                </View>
            ) : null,
            headerRight: () => !this.state.isInfoToken ? (
                <TouchableOpacity onPress={this.omitir}>
                    <Text style={[header.headerTextStyle, { color: '#0000EE' }]}>Omitir</Text>
                </TouchableOpacity>
            ) : null
        });

    }

    saveChanges = async () => {
        let user = this.state.user;
        console.log('State user: ', user)
        let updated = await updateUser(user.id, user);
        if (updated && updated.code === 200 && updated.status === "success") {
            console.log("Updating localdata ", user);
            localAPI.saveUser(user);
            Alert.alert("Cambios guardados", "Tus cambios fueron guardados satisfactoriamente.");
            if (!this.state.userToken) {
                this.goToStartSession();
            }
        }
        else {
            Alert.alert("Error", "Hubo un problema al guardar los datos.");
        }
    }

    goToStartSession = () => {

        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [
                    { name: 'StartSession' }
                ]
            })
        );
    }

    onChangeText = (value) => (text) => {
        let user = {
            ...this.state.user
        };
        user[value] = text;
        this.setState({ user });
    }

    onValueChange = (value) => (text) => {
        let user = {
            ... this.state.user
        };
        user[value] = text;
        this.setState({ user });
    }

    render() {

        const { user } = this.state;

        return (
            <SafeAreaView style={containers.mainContainer}>

                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={[containers.headerContainer, { height: '20%' }]}>
                        <View>
                            <Image source={require("../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '75%' }]} />
                        </View>
                    </View>

                    <View style={styles.bodyContainer}>
                        <View>
                            <TextInput
                                label="Edad"
                                value={user.age}
                                onChangeText={this.onChangeText("age")}
                                keyboardType='numeric'
                                maxLength={2}
                                style={styles.textInput}
                                underlineColor="#FF2D59"
                            />
                        </View>

                        <View>
                            <TextInput
                                label="Altura (Cm)"
                                value={user.height}
                                onChangeText={this.onChangeText("height")}
                                keyboardType='numeric'
                                maxLength={5}
                                style={styles.textInput}
                                underlineColor="#FF2D59"
                            />
                        </View>

                        <View>
                            <TextInput
                                label="Peso (kg)"
                                value={user.weight}
                                onChangeText={this.onChangeText("weight")}
                                keyboardType='numeric'
                                maxLength={3}
                                style={styles.textInput}
                                underlineColor="#FF2D59"
                            />
                        </View>

                        <View>
                            <TextInput
                                label="¿Padece algún problema de salud?"
                                onChangeText={this.onChangeText("conditions")}
                                value={user.conditions}
                                style={{ width: 280 }}
                                multiline={true}
                                underlineColor="#FF2D59"
                            />
                        </View>


                        <View style={{ justifyContent: 'center', alignContent: 'center' }}>
                            <RadioButton.Group onValueChange={this.onValueChange("gender")} value={user.gender}>
                                <View style={styles.radioButtons}>
                                    <View style={styles.textView}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>¿Cual es tu género:</Text>
                                    </View>
                                </View>
                                <View style={styles.radioButtons}>
                                    <RadioButton value="1" color="#FF2D59" />
                                    <View style={styles.textView}>
                                        <Text>Masculino</Text>
                                    </View>
                                </View>
                                <View style={styles.radioButtons}>
                                    <RadioButton value="0" color="#FF2D59" />
                                    <View style={styles.textView}>
                                        <Text>Femenino</Text>
                                    </View>
                                </View>
                            </RadioButton.Group>
                        </View>

                        <View>
                            <CustomButton
                                title="Guardar"
                                onPress={this.saveChanges}
                                style={buttons.primaryButton}
                                textStyle={texts.primaryText}
                            />
                        </View>
                    </View>



                </ScrollView>

            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    bodyContainer: {
        width: '100%',
        height: '80%',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    textInput: {
        width: 180,
        height: 50
    },
    radioButtons: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
    },
    textView: {
        justifyContent: 'center'
    },
    textAreaContainer: {
        backgroundColor: '#FFF',
        borderWidth: 0.8,
        borderRadius: 8,
        borderColor: 'grey',
        width: 300,
        height: 300
    },
    textArea: {
        height: 300,
        margin: 15,
        fontSize: 16,
        justifyContent: "flex-start",
        textAlignVertical: 'top',
    },
});
