import React, { Component } from 'react';
import { Dimensions, View, Image, FlatList, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { TextInput, RadioButton, Text } from 'react-native-paper';
import { containers, header, images } from '../../styles/CommonStyle';
import Loader from '../../Components/Common/Loader';
import { Card } from 'react-native-paper';
import { getArticles } from '../../utils/API';

export default class ProfileField extends Component {

    render(){
        return (
                <TextInput
                    label={this.props.title}
                    onChangeText={this.props.update}
                    value={this.props.title}
                    style={{ width: 350 }}
                    underlineColor="#FF2D59"
                />
            )
    }
}