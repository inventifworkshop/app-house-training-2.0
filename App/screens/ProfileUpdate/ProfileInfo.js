import React, { Component } from 'react';
import { Dimensions, View, Text, Image, FlatList, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { containers, header, images } from '../../styles/CommonStyle';
import Loader from '../../Components/Common/Loader';
import { Card } from 'react-native-paper';
import { getArticles } from '../../utils/API';
import ProfileField from './ProfileField';
import { update } from 'tcomb-form-native/lib';
export default class Menu extends Component {

    state = {
        updates: {

        }
    }

    async componentDidMount() {
        this.setHeader();
        this.getArticles();
    }

    setHeader = () => {
        this.props.navigation.setOptions({
            title: 'Alimentación'
        })
    }

    getArticles = async () => {
        let categories = await getArticles();
        this.setState({ articles: categories.articles, isArticles: true });
    }

    goToArticle = (id) => () => {
        this.props.navigation.navigate("Article", { id: id });
    }

    goToArticlesByCategory = (value) => {
        this.props.navigation.navigate('ArticlesByCategory', { id: value });
    }

    addPropertyToUpdate =(property,  value) =>{
        this.setState(prevState=>{
            let updates = {...prevState.updates};
            updates[property]=value;
            return {
                updates: updates
            };
        });
    }

    render() {
        return (
            <View style={containers.mainContainer}>
                <ScrollView style={{ width: "100%", height: "100%" }}>
                    <Text>Mi informacón</Text>
                    <View style={{display:'flex', flexDirection:"column"}}>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Nombre" property="name" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Apellido" property="last_name" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Telefono" property="phone" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Direccion" property="address" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Edad" property="age" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="E-mail" property="email" update={addPropertyToUpdate}/>
                        </View>
                        <View style={{flex:'1'}}>
                          <ProfileField title="Gender" property="gender" update={addPropertyToUpdate}/>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
    item: {
        paddingHorizontal: 4,
        paddingVertical: 8
    },
    imageStyle: {
        width: (windowWidth - 22) / 1.5,
        height: 150,
        resizeMode: 'cover',
    },
});