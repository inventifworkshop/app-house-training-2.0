import React, { Component } from 'react';
import { Dimensions, View, Text, Image, FlatList, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { containers, header, images } from '../../styles/CommonStyle';
import Loader from '../../Components/Common/Loader';
import { Card } from 'react-native-paper';
import { getArticles } from '../../utils/API';

export default class Menu extends Component {

    state = {
        articles: {},
        isArticles: false
    }

    async componentDidMount() {
        this.setHeader();
        this.getArticles();
    }

    setHeader = () => {
        this.props.navigation.setOptions({
            title: 'Alimentación'
        })
    }

    getArticles = async () => {
        let categories = await getArticles();
        this.setState({ articles: categories.articles, isArticles: true });
    }

    goToArticle = (id) => () => {
        this.props.navigation.navigate("Article", { id: id });
    }

    goToArticlesByCategory = (value) => {
        this.props.navigation.navigate('ArticlesByCategory', { id: value });
    }

    render() {

        const { articles, isArticles } = this.state;

        return (
            <View style={containers.mainContainer}>
                <ScrollView style={{ width: "100%", height: "100%" }}>

                    <View style={containers.headerContainer}>
                        <Image source={require('../../../assets/icons/salad.png')} resizeMode="contain" style={[images.logo, { width: '60%' }]} />
                    </View>

                    {isArticles ?

                        <View style={{ flex: 5 }}>
                            {Object.keys(articles).map((key) => {

                                var category_name = articles[key].category_name;

                                return (
                                    <View style={{ padding: 4 }}>
                                        <TouchableOpacity style={{ paddingVertical: 4 }} onPress={() => {
                                            this.goToArticlesByCategory(articles[key].category_id)
                                        }}>
                                            <Text style={[header.headerTextStyle, { alignSelf: "flex-start", color: '#0000EE' }]}>
                                                {category_name}
                                            </Text>
                                        </TouchableOpacity>
                                        <FlatList
                                            data={articles[key].category_articles}
                                            renderItem={(item) =>
                                                <TouchableOpacity onPress={this.goToArticle(item.item.id)} style={styles.item}>
                                                    <Card>
                                                        <Card.Title title={item.item.name} titleStyle={{ fontSize: 15 }} />
                                                        <Card.Cover
                                                            style={styles.imageStyle}
                                                            source={
                                                                {
                                                                    uri: item.item.main_image ? item.item.main_image : `https://picsum.photos/200/300?grayscale`
                                                                }
                                                            }
                                                        />
                                                    </Card>
                                                </TouchableOpacity>
                                            }
                                            keyExtractor={item => "keyi" + item.id}
                                            horizontal
                                        />
                                    </View>
                                )
                            })}
                        </View>
                        :
                        <View style={{ padding: 150 }}>
                            <Loader />
                        </View>
                    }
                </ScrollView>
            </View>
        )
    }
}

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
    item: {
        paddingHorizontal: 4,
        paddingVertical: 8
    },
    imageStyle: {
        width: (windowWidth - 22) / 1.5,
        height: 150,
        resizeMode: 'cover',
    },
});