import React, { Component } from 'react';
import { View, Text, Image, Modal } from 'react-native';
import { WebView } from 'react-native-webview';
import { requestArticle, downloadArticlePDF } from '../../utils/API';
import IconButton from '../../Components/Common/IconButton.js';
import { containers } from '../../styles/CommonStyle';

export default class Articles extends Component {

    constructor(props) {
        super(props);
        this.state = {
            articleContent: "",
            articleTitle: "",
            articleImage: undefined,
            downloading: false
        }
    }

    async componentDidMount() {
        this.setHeader();

        let idArticle = this.props.route.params.id;
        let result = await requestArticle(idArticle);
        console.log('Data: ', result);
        console.log("The article response was ", result.content);
        let page = '<!doctype html>' +
            '<html lang="en">' +
            '<head>' +
            '<!-- Required meta tags -->' +
            '<meta charset="utf-8">' +
            '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">' +

            '<!-- Bootstrap CSS -->' +
            '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">' +

            '<title>Hello, world!</title>' +
            '</head>' +
            '<body>' + result.content + '<!-- Optional JavaScript -->' +
            '<!-- jQuery first, then Popper.js, then Bootstrap JS -->' +
            '<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>' +
            '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>' +
            '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>' +
            '</body>' +
            '</html>';
        this.setState({ articleContent: page, articleTitle: result.name, articleImage: result.main_image });
    }

    downloadFile = async () => {
        this.setState({ downloading: true });
        let idArticle = this.props.route.params.id;
        downloadArticlePDF(idArticle);
        this.setState({ downloading: false });
    }

    goToCalendar = () => {
        let articleId = this.props.navigation.getParam("url", 0);
        //this.props.navigation.navigate("Calendar", { id: articleId });
    }

    setHeader = () => {
        this.props.navigation.setOptions({
            title: 'Alimentación'
        })
    }

    render() {

        return (
            <View style={containers.mainContainer}>

                <View style={{ flex: 3, width: "100%", flexDirection: "row" }}>
                    <Image style={{ width: 100, height: 100, marginLeft: "auto", marginRight: "auto" }} source={require('../../../assets/icons/salad.png')} />
                </View>
                <View style={{ flex: 2, width: "100%", flexDirection: "row", paddingHorizontal: 15 }}>
                    <View style={{ flex: 1 }}>
                        <IconButton onPress={this.goToCalendar} source={require("../../../assets/icons/calendar.png")}></IconButton>
                    </View>
                    <View style={{ flex: 8 }}>
                    </View>
                    <View style={{ flex: 1 }}>
                        <IconButton onPress={this.downloadFile} source={require("../../../assets/icons/download.png")}></IconButton>
                    </View>
                </View>
                <View style={{ flex: 15, width: "100%", backgroundColor: "blue" }}>
                    <WebView source={{ html: (this.state.articleContent || "") }} />
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.downloading}>
                    <View>
                        <Text>Downloading pdf file </Text>
                    </View>
                </Modal>
            </View>
        )
    }
}
