import React, { Component } from 'react';
import { Dimensions, View, Text, Image, FlatList, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { containers, header, images } from '../../styles/CommonStyle';
import Loader from '../../Components/Common/Loader';
import { Card } from 'react-native-paper';
import { getArticlesByCategory } from '../../utils/API';

export default class ArticlesByCategory extends Component {

    state = {
        articles: {},
        category: {},
        isArticles: false,
    }

    async componentDidMount() {
        this.setHeader();

        let idCategory = this.props.route.params.id;
        this.getArticlesByCategory(idCategory);
    }

    setHeader = () => {
        this.props.navigation.setOptions({
            title: 'Alimentación'
        })
    }

    getArticlesByCategory = async (value) => {
        let categories = await getArticlesByCategory(value);
        console.log('Data: ', categories);
        this.setState({ articles: categories.articles, isArticles: true });
        console.log('State: ', this.state.articles);
    }

    goToArticle = (id) => () => {
        this.props.navigation.navigate("Article", { id: id });
    }

    render() {

        const { articles, isArticles } = this.state;

        return (
            <View style={containers.mainContainer}>

                <View style={containers.headerContainer}>
                    <Image source={require('../../../assets/icons/salad.png')} resizeMode="contain" style={[images.logo, { width: '60%' }]} />
                </View>

                {isArticles ?
                    <View style={{ flex: 5 }}>

                        <View style={{ padding: 4 }}>
                            <FlatList
                                data={articles.data}
                                renderItem={(item) =>
                                    <TouchableOpacity onPress={this.goToArticle(item.item.id)} style={styles.item}>
                                        <Card>
                                            <Card.Title title={item.item.name} titleStyle={{ fontSize: 16 }} />
                                            <Card.Cover
                                                style={styles.imageStyle}
                                                source={
                                                    {
                                                        uri: item.item.main_image ? item.item.main_image : `https://picsum.photos/200/300?grayscale`
                                                    }
                                                }
                                            />
                                        </Card>
                                    </TouchableOpacity>
                                }
                                keyExtractor={item => "keyi" + item.id}
                            />
                        </View>

                    </View>
                    :
                    <View style={{ padding: 150 }}>
                        <Loader />
                    </View>
                }
            </View>
        )

    }

}

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
    item: {
        paddingHorizontal: 5,
        paddingVertical: 6
    },
    imageStyle: {
        width: (windowWidth - 32),
        height: 150,
        resizeMode: 'cover',
    },
});
