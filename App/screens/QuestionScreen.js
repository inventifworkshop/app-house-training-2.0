import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { containers } from '../styles/CommonStyle';
import localAPI from '../utils/LocalAPI';
import { getQuestions } from '../utils/API';
import { WebView } from 'react-native-webview';

export default class QuestionScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //content: "",
            user: {}
        }
    }

    async componentDidMount() {

        let user = await localAPI.getUser();
        this.setState({ user: user });
        /*
        let result = await getQuestions(user.id);
        console.log('Data: ', result);
        console.log("The page response was ", result.content);
        let page = '<!doctype html>' +
            '<html lang="en">' +
            '<head>' +
            '<!-- Required meta tags -->' +
            '<meta charset="utf-8">' +
            '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">' +

            '<!-- Bootstrap CSS -->' +
            '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">' +

            '<title>House Training - Inventif</title>' +
            '</head>' +
            '<body>' + result.content + '<!-- Optional JavaScript -->' +
            '<!-- jQuery first, then Popper.js, then Bootstrap JS -->' +
            '<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>' +
            '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>' +
            '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>' +
            '</body>' +
            '</html>';
        this.setState({ content: page, articleTitle: result.name, articleImage: result.main_image });
        */
    }

    render() {

        const { user } = this.state;
        return (
            <View style={containers.mainContainer}>
                <View style={{ flex: 15, width: "100%", backgroundColor: "blue" }}>
                    <WebView
                        source={{ uri: 'https://services.housetrainings.com/questionsmobile/' + user.id }}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})
