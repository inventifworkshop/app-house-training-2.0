import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import { buttons, containers, header, images, texts } from '../../styles/CommonStyle';
import { CustomButton } from '../../Components/Common/CustomButton';
import Loader from '../../Components/Common/Loader';
import localAPI from '../../utils/LocalAPI';
import { Utils } from '../../Helpers'

export default class StartSession extends Component {

    state = {
        user: {},
        loggedIn: true,
        isUserDeleted: false,
    }

    async componentDidMount() {
        await this.updateData();
    }

    updateData = async () => {

        let user = await localAPI.getUser();
        console.log("Recover local user ", user);

        if (user) {
            this.setState({ loggedIn: true, user: user });
            const userToken = await AsyncStorage.getItem('userToken');
            if (!userToken) {
                Utils.createKeySession('userToken');
                const userToken = await AsyncStorage.getItem('userToken');
                console.log('UserToken created(StartSession): ', userToken);
            }
            console.log("User validated: ", this.state)
        }

        let value = this.props.route.params?.deleted ?? false;
        console.log('Value: ', value);

        this.setHeader(value);
        this.setState({ isUserDeleted: value });
    }

    setHeader = (value) => {

        if (value) {
            this.props.navigation.setOptions({
                headerLeft: null
            });
        } else {
            this.props.navigation.setOptions({
                headerLeft: () =>
                    <View style={header.headerLeft}>
                        <TouchableOpacity onPress={this.goToConfiguration}>
                            <Image source={require('../../../assets/config.png')}
                                style={header.headerButtons}
                            />
                        </TouchableOpacity>
                    </View>
            })
        }
    }

    goToSessionPreview = () => {
        this.props.navigation.navigate("SessionPreview");
    }

    goToMenu = () => {
        this.props.navigation.navigate("Menu");
    }

    goToConfiguration = () => {
        this.props.navigation.navigate("Config");
    }

    showAlert = () => {
        Alert.alert('Aviso', 'De momento seguimos trabajando en esta función');
    }

    render() {
        return (

            this.state.loggedIn ?

                <View style={containers.mainContainer}>
                    <View style={[containers.headerContainer, { height: '35%' }]}>

                        <View>
                            <Image source={require("../../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '80%' }]} />
                        </View>

                        {this.state.isUserDeleted ?
                            <View />
                            :
                            <View style={styles.textContainer}>
                                <Text style={texts.title}>
                                    Bienvenido {this.state.user.name || ""}
                                </Text>
                            </View>
                        }
                    </View>

                    {
                        !this.state.isUserDeleted ?

                            <View style={styles.optionsContainer}>
                                <View>
                                    <CustomButton
                                        title="Enseñanza"
                                        onPress={this.showAlert}
                                        style={buttons.secondaryButton}
                                    />
                                </View>

                                <View>
                                    <CustomButton
                                        title="Alimentación"
                                        onPress={this.goToMenu}
                                        style={buttons.secondaryButton}
                                    />
                                </View>

                                <View style={{ paddingTop: 15 }}>
                                    <CustomButton
                                        title="Entrenamiento"
                                        onPress={this.goToSessionPreview}
                                        style={buttons.primaryButton}
                                        textStyle={texts.primaryText}
                                    />
                                </View>

                                <View style={{ paddingTop: 15 }}>
                                    <CustomButton
                                        title="Calendario"
                                        onPress={this.showAlert}
                                        style={buttons.primaryButton}
                                        textStyle={texts.primaryText}
                                    />
                                </View>
                            </View>
                            :
                            <View style={styles.restrictionContainer}>
                                <View>
                                    <Text style={styles.textRestriction}>
                                        Su cuenta ha sido dada de baja, gracias por usar el servicio, en cualquier momento puede volver a ingresar a la aplicación, ingresando con su correo y contraseña.
                                    </Text>
                                </View>
                            </View>
                    }
                </View>
                :
                <Loader />
        )
    }
}

const styles = StyleSheet.create({
    optionsContainer: {
        width: '100%',
        height: '65%',
        alignItems: 'center',
    },
    restrictionContainer: {
        width: '100%',
        height: '65%',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    textRestriction: {
        textAlign: 'justify',
        fontSize: 17
    },
    textContainer: {
        justifyContent: 'space-around',
        alignSelf: 'flex-start',
        marginLeft: 20,
    },

})