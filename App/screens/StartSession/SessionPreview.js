import React, { Component } from 'react';
import { View, Modal, Text, Image, ScrollView, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { buttons, containers, images, header, texts } from '../../styles/CommonStyle';
import { getTodaySessions } from '../../utils/API';
import { CustomButton } from '../../Components/Common/CustomButton';
import { Validations } from '../../Helpers';
import { Button, TextInput } from 'react-native-paper';
import localAPI from '../../utils/LocalAPI';
import { Utils } from '../../Helpers';

export default class SessionPreview extends Component {

  state = {
    sessionDetails: undefined,
    exercises: [],
    exercisesAvailable: [],
    previewExercises: [],
    user: {},
    time: '',
    indexExercise: null,
    notSession: false,
    modalVisible: false,
    modalExerciseVisible: false
  }

  async componentDidMount() {

    let user = await localAPI.getUser();
    this.setState({ user: user });

    let sessions = await getTodaySessions(user.id);

    if (sessions.code === 404) {
      this.setState({ notSession: true });
    } else {
      //Cheap trick to accomplish "cycles", replace with something better later
      let sessionDetails = sessions.routine;
      let previewVersion = [...sessions.routine.exercises];
      let fullExercises = [];
      for(let x=0;x<sessionDetails.cycles;x++){
        fullExercises.push(...sessionDetails.exercises);
      }
      
      sessionDetails.exercises = fullExercises;
      //

      this.setState({ sessionDetails: sessions.routine });
      this.setState({ exercises: Utils.getExercises(sessions.routine) });
      this.setState({ exercisesAvailable: Utils.getExercises(sessions.routine),
         previewExercises: Utils.getExercises({exercises:previewVersion}) })
    }

    this.setHeader();
  }

  setHeader = () => {
    this.props.navigation.setOptions({
      headerLeft: () =>
        <View style={header.headerLeft}>
          <TouchableOpacity onPress={this.goToConfiguration}>
            <Image source={require('../../../assets/config.png')}
              style={header.headerButtons}
            />
          </TouchableOpacity>
        </View>
      ,
      headerRight: () =>
        <View style={header.headerRight}>
          <TouchableOpacity onPress={this.goBack}>
            <Image source={require('../../../assets/back.png')}
              style={header.headerButtons}
            />
          </TouchableOpacity>
        </View>
    });

  }

  goToConfiguration = () => {
    this.props.navigation.navigate("Config");
  }

  goBack = () => {
    this.props.navigation.goBack();
  }

  goToExerciseInfo = (value) => {
    this.props.navigation.navigate("SessionInfo", { sessionData: this.state.exercises[value] });
  }

  getExerciseList = () => { 
    let result = this.state.previewExercises.map((exerciseItem, index) => {
      let exercise = exerciseItem;
      return (
        <View>
          <CustomButton
            title={Validations.validateString(exercise.name)}
            style={[buttons.secondaryButton, (this.state.modalVisible || this.state.modalExerciseVisible) ? { backgroundColor: 'rgba(0,0,0,0.2)' } : '#EEEEEE']}
            key={index}
            onPress={() => this.goToExerciseInfo(index)}
            onLongPress={() => this.setModalExerciseVisible(true, index)}
          />
        </View>
        
      )
    }
  );

  return result;
  }

  goToWarmUp = () => {
    this.props.navigation.navigate("SessionWarmup", { id: this.state.user.id });
  }

  goToExerciseStart = () => {
    this.props.navigation.navigate("SessionView", { sessionData: this.state.sessionDetails });
  }

  saveTime = (text) => {
    const { modalVisible } = this.state;
    let sessionDetails = {
      ...this.state.sessionDetails
    };

    if (this.state.time > 15) {
      sessionDetails[text] = this.state.time;
      this.setState({ sessionDetails });
      this.setModalVisible(!modalVisible);
    } else {
      Alert.alert('Aviso', 'Los segundos deben ser mayor a 15.');
    }
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }

  setModalExerciseVisible = (visible, key) => {
    this.setState({ indexExercise: key, modalExerciseVisible: visible });
  }

  changeExercise = (index) => {
    const { exercises, exercisesAvailable, modalExerciseVisible, indexExercise } = this.state;

    exercises[indexExercise] = exercisesAvailable[index];

    const newExercises = [...this.state.exercisesAvailable];
    newExercises.splice(index, 1);

    this.setState({
      exercisesAvailable: newExercises
    });

    this.setModalExerciseVisible(!modalExerciseVisible);
  }

  render() {

    const { modalVisible, modalExerciseVisible } = this.state;

    return (
      <View style={[containers.mainContainer, (modalVisible || modalExerciseVisible) ? { backgroundColor: 'rgba(0,0,0,0.5)' } : '#FFF']}>


        {/**VENTANA MODAL PARA CAMBIAR EL TIEMPO DE DESCANSO */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => this.setModalVisible(!modalVisible)}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={{ marginBottom: 15 }}>
                <Text style={styles.modalText}>Ingrese el tiempo de descanso</Text>
              </View>
              <View style={{ marginBottom: 15 }}>
                <Image source={require("../../../assets/icons/rest.png")} resizeMode="contain" style={[images.logo, { width: '80%' }]} />
              </View>
              <View style={{ marginBottom: 15 }}>
                <TextInput
                  label="Segundos"
                  value={this.state.time}
                  onChangeText={(value) => this.setState({ time: value })}
                  maxLength={2}
                  keyboardType='numeric'
                  style={styles.textInput}
                  underlineColor="#FF2D59"
                />
              </View>
              <View>
                <CustomButton
                  title="Guardar"
                  onPress={() => this.saveTime('rest_exercise')}
                  style={[buttons.primaryButton, { backgroundColor: '#06bcee' }]}
                  textStyle={texts.primaryText}
                />
              </View>
            </View>
          </View>
        </Modal>

        {/**VENTANA MODAL PARA CAMBIAR EL EJERCICIO */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalExerciseVisible}
          onRequestClose={() => this.setModalExerciseVisible(!modalExerciseVisible)}
        >
          <View style={styles.centeredView}>
            <View style={[styles.modalView, { width: '90%' }]}>
              <View style={{ marginBottom: 15 }}>
                {this.state.exercisesAvailable.length == 0 ?
                  <View style={{ alignItems: "center" }}>
                    <Text style={[texts.title, { fontSize: 18 }]}>¡No hay ejercicios disponibles!</Text>
                  </View>
                  :
                  <View style={{ alignItems: "center" }}>
                    <Text style={[texts.title, { fontSize: 18 }]}>Ejercicios disponibles</Text>
                  </View>
                }
              </View>
              <View style={{ marginBottom: 15 }}>
                {this.state.exercisesAvailable.length == 0 ?
                  <View style={{ alignItems: "center" }}>
                    <Text>Aún no cuentas con ejercicios para reemplazar.</Text>
                  </View>
                  :
                  this.state.exercisesAvailable && this.getExerciseList()
                }
              </View>
            </View>
          </View>
        </Modal>

        <ScrollView style={styles.scrollview}>

          <View style={[containers.headerContainer, (modalVisible || modalExerciseVisible) ? { backgroundColor: 'rgba(0,0,0,0.2)' } : '#FFF']}>
            <Image source={require("../../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '80%' }]} />
          </View>

          {!this.state.sessionDetails && !this.state.notSession && <Loader />}
          {this.state.sessionDetails &&
            <View style={[containers.mainContainer, (modalVisible || modalExerciseVisible) ? { backgroundColor: 'rgba(0,0,0,0.2)' } : '#FFF', { alignContent: 'space-around' }]}>

              <View style={styles.container}>
                <View style={styles.buttonContainer}>
                  <Button icon={require('../../../assets/icons/exercises.png')} style={styles.buttons} labelStyle={{ fontSize: 18 }} mode="text" color='#000' uppercase={false}>
                    {this.state.sessionDetails.cycles + ' Ciclos'}
                  </Button>
                  <Button icon={require('../../../assets/icons/watch.png')} style={styles.buttons} labelStyle={{ fontSize: 18 }} mode="text" color='#000' uppercase={false}>
                    Intervalo
                  </Button>
                </View>

                <View style={styles.buttonContainer}>
                  <Button icon={require('../../../assets/icons/rest.png')} style={styles.buttons} labelStyle={{ fontSize: 18 }} mode="text" color='#000' uppercase={false} onPress={() => this.setModalVisible(true)}>
                    {this.state.sessionDetails.rest_exercise + 's' + ' descanso'}
                  </Button>
                  <Button icon={require('../../../assets/icons/warmup.png')} style={styles.buttons} labelStyle={{ fontSize: 18 }} mode="text" color='#000' uppercase={false} onPress={() => this.goToWarmUp()}>
                    Calentamiento
                  </Button>
                </View>
              </View>

              <View>
                {this.state.exercises && this.getExerciseList()}
              </View>

              <View style={styles.exercisesContainer}>
                <CustomButton
                  title="Adelante!"
                  onPress={this.goToExerciseStart}
                  style={[buttons.primaryButton, (modalVisible || modalExerciseVisible) ? { backgroundColor: 'rgba(0,0,0,0.5)' } : '#FFF']}
                  textStyle={texts.primaryText}
                />
              </View>

            </View>
          }
          {this.state.notSession &&
            <View style={containers.mainContainer}>
              <View style={{ flex: 4, alignItems: "center" }}>
                <Text style={texts.title}>¡Aún no tienes una sesión!</Text>
                <Text>La sesión de hoy aún no está disponible {"\n"}Por favor, inténtelo de nuevo más tarde.</Text>
              </View>
            </View>
          }
        </ScrollView>
      </View >
    )
  }
}

const styles = StyleSheet.create({
  scrollview: {
    width: '100%',
    height: '100%'
  },
  exercisesContainer: {
    width: '100%',
    paddingVertical: 15,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
  },
  buttons: {
    height: 65,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textInput: {
    width: 180,
    height: 50
  },

  //Modals
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    width: '75%',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalText: {
    marginBottom: 15,
    fontWeight: 'bold',
    textAlign: "center"
  },
})