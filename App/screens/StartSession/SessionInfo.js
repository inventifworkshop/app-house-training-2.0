import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native';
import { containers, images, header, texts } from '../../styles/CommonStyle';


export default class SessionView extends Component {

    state = {
        exercise: [],
    }

    async componentDidMount() {
        let exerciseInfo = [];
        exerciseInfo = this.props.route.params.sessionData;

        console.log("Exercise: ", exerciseInfo);
        this.setState({ exercise: exerciseInfo })

        this.setHeader();
    }

    setHeader = () => {
        this.props.navigation.setOptions({
            headerLeft: () =>
                <View style={header.headerLeft}>
                    <TouchableOpacity onPress={this.goToConfiguration}>
                        <Image source={require('../../../assets/config.png')}
                            style={header.headerButtons}
                        />
                    </TouchableOpacity>
                </View>
            ,
            headerRight: () =>
                <View style={header.headerRight}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Image source={require('../../../assets/back.png')}
                            style={header.headerButtons}
                        />
                    </TouchableOpacity>
                </View>
        });
    }

    goToConfiguration = () => {
        this.props.navigation.navigate("Config");
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={containers.mainContainer}>

                <View style={containers.headerContainer}>
                    <Image source={require("../../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '80%' }]} />
                </View>

                <View style={{ display: "flex", flex: 1, flexDirection: "column" }}>

                    <View style={{ flex: 7 }}>
                        <Image resizeMode="contain" style={images.image} source={this.state.exercise.image ? { uri: this.state.exercise.image.replace(";", "") } : require("../../../assets/Splash_2.png")} />
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={texts.textName}>{this.state.exercise.name}</Text>
                        <Text style={texts.textDescription}>{this.state.exercise.description}</Text>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    infoContainer: {
        flex: 3,
        marginVertical: 15,
        marginHorizontal: 15,
    },
});
