import React, { Component } from 'react';
import { View, Modal, Text, Image, ScrollView, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { buttons, containers, images, header, texts } from '../../styles/CommonStyle';
import { getWarmUpExercises } from '../../utils/API';
import { Utils } from '../../Helpers';
import { CustomButton } from '../../Components/Common/CustomButton';
import { Validations } from '../../Helpers';


export default class SessionWarmup extends Component {

    state = {
        sessionDetails: undefined,
        notSession: false,
        exercises: [],
    }

    async componentDidMount() {

        const { params } = this.props.route;
        userId = params ? params.id : null;

        let sessions = await getWarmUpExercises(userId);

        if (sessions.code === 404) {
            this.setState({ notSession: true });
        } else {
            this.setState({ sessionDetails: sessions.routine });
            this.setState({ exercises: Utils.getExercises(sessions.routine) });
        }

        this.setHeader();

    }

    setHeader = () => {
        this.props.navigation.setOptions({
            headerLeft: () =>
                <View style={header.headerLeft}>
                    <TouchableOpacity onPress={this.goToConfiguration}>
                        <Image source={require('../../../assets/config.png')}
                            style={header.headerButtons}
                        />
                    </TouchableOpacity>
                </View>
            ,
            headerRight: () =>
                <View style={header.headerRight}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Image source={require('../../../assets/back.png')}
                            style={header.headerButtons}
                        />
                    </TouchableOpacity>
                </View>
        });

    }

    goToConfiguration = () => {
        this.props.navigation.navigate("Config");
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    goToExerciseInfo = (value) => {
        this.props.navigation.navigate("SessionInfo", { sessionData: this.state.exercises[value] });
    }

    goToExerciseStart = () => {
        this.props.navigation.navigate("SessionView", { sessionData: this.state.sessionDetails });
    }

    render() {

        return (
            <View style={containers.mainContainer}>

                <ScrollView style={{ width: "100%", height: "100%" }}>

                    <View style={containers.headerContainer}>
                        <Image source={require("../../../assets/logo.png")} resizeMode="contain" style={[images.logo, { width: '80%' }]} />
                    </View>

                    {!this.state.sessionDetails && !this.state.notSession && <Loader />}
                    {this.state.sessionDetails &&
                        <View style={containers.mainContainer}>

                            <View>
                                {this.state.exercises && this.state.exercises.map((exercise, index) => {
                                    return (
                                        <CustomButton
                                            title={Validations.validateString(exercise.name)}
                                            style={buttons.secondaryButton}
                                            key={index}
                                            onPress={() => this.goToExerciseInfo(index)}
                                        />
                                    )
                                })}
                            </View>

                            <View style={styles.exercisesContainer}>
                                <CustomButton
                                    title="Adelante!"
                                    onPress={this.goToExerciseStart}
                                    style={buttons.primaryButton}
                                    textStyle={texts.primaryText}
                                />
                            </View>

                        </View>
                    }
                    {this.state.notSession &&
                        <View style={CommonStyle.mainContainer}>
                            <View style={{ flex: 4, alignItems: "center" }}>
                                <Text style={CommonStyle.title}>¡Aún no tienes una sesión!</Text>
                                <Text>La sesión de hoy aún no está disponible {"\n"}Por favor, inténtelo de nuevo más tarde.</Text>
                            </View>
                        </View>
                    }
                </ScrollView>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    exercisesContainer: {
        width: '100%',
        paddingVertical: 15,
        alignItems: 'center',
    },
})