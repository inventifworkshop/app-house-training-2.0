import { StyleSheet } from 'react-native'

const containers = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: "#FFF",
  },
  headerContainer: {
    width: '100%',
    alignItems: 'center',
  },
  imageContainer: {
    width: '100%',
    height: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subContainer: {
    width: '100%',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: "#6766D6",
  },
  hairline: {
    backgroundColor: '#EEEEEE',
    height: '6%',
    width: '100%'
  },
});

const buttons = StyleSheet.create({
  subButton: {
    height: 45,
    width: 320,
    borderColor: '#FFF',
    borderRadius: 8,
    marginVertical: 10,
    borderWidth: 1,
  },
  primaryButton: {
    height: 50,
    width: 180,
    backgroundColor: '#FF2D59',
    borderRadius: 5,
  },
  secondaryButton: {
    height: 50,
    width: 300,
    borderColor: '#171717',
    backgroundColor: '#EEEEEE',
    borderRadius: 18,
    marginVertical: 10,
    borderWidth: 1,
  },
  styleButton: {
    height: 35,
    width: '88%',
    alignItems: 'flex-start',
    borderColor: '#FF2D59',
    borderBottomWidth: 2,
    marginTop: 10,
  },
});

const images = StyleSheet.create({
  logo: {
    width: '95%',
    height: undefined,
    aspectRatio: 2,
  },
  image: {
    width: "100%",
    height: "100%"
  },
});

const texts = StyleSheet.create({
  primaryText: {
    color: '#FFF',
    fontSize: 17,
  },
  textName: {
    fontSize: 22,
    fontWeight: "bold",
  },
  textDescription: {
    fontSize: 15,
    textAlign: 'justify',
    color: '#808080'
  },
  title: {
    color: "#343434",
    fontSize: 22,
    fontWeight: "bold",
    alignSelf: "center"
  },
  bigText: {
    color: "#FF2D59",
    fontSize: 50,
    fontWeight: "bold",
  },
});

const header = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#fff',
    elevation: 0
  },
  headerTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginRight: 20,
    textDecorationLine: 'underline',
  },
  headerTitleStyle: {
    fontSize: 20,
  },
  headerLeft: {
    marginLeft: 10
  },
  headerRight: {
    marginRight: 12,
  },
  headerButtons: {
    width: 50,
    height: 40,
    marginVertical: 5,
    resizeMode: 'contain',
  },
});

const inputs = StyleSheet.create({
  defaultInput:{
    borderColor:"#555555",
    borderWidth:0,
    borderTopWidth:1
  }
})

export { containers, buttons, images, texts, header }