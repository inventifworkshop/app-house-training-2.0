exports.formatToDate= (value) =>{
  let response="00-00";
  if(value){ 
    let splitted = value.split("-");
    response=splitted[1]+"-"+splitted[2];
  }
  return response;
}

exports.formatToTime = (value) =>{
  let response="0:00AM";
  let affix="AM";
  if(value){ 
    let splitted = value.split(":");
    if(splitted[0]>12){
      splitted[0]-=12;
      affix="PM";
    }
    response= splitted[0]+":"+splitted[1]+affix;
  }
  return response;
}