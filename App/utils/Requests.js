import * as FileSystem from 'expo-file-system';


exports.getRequest = async function (url) {
  try {
    const response = await fetch(url);
    const responseJson = await response.json();
    return responseJson;
  }
  catch (error) {
    console.error(error);
  }
}

exports.postRequest = async function (url, data) {
  console.log("Start requesting");
  let requestData = {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const response = await fetch(url, requestData);
  const responseJson = await response.json();
  console.log("POST RESPONSE:  ", responseJson);
  return responseJson
}

exports.putRequest = async function (url, data) {
  console.log("Starting put request");
  let requestData = {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const response = await fetch(url, requestData);
  const responseJson = await response.json();
  console.log("PUT RESPONSE:  ", responseJson);
  return responseJson
}

exports.deleteRequest = async function (url) {
  console.log("Starting delete request");
  let requestData = {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const response = await fetch(url, requestData);
  const responseJson = await response.json();
  console.log("DELETE RESPONSE:  ", responseJson);
  return responseJson
}

exports.getDownloadToFile = async function (url, filename,callback, dir=""){
  console.log("Downloading ",url);
  const downloadResumable = FileSystem.createDownloadResumable(
    url,
    FileSystem.documentDirectory + filename,
    {},
    callback
  );
  try {
    const { uri } = await downloadResumable.downloadAsync();
    console.log('Finished downloading to ', uri);
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}