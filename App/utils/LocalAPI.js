import localStorage from './LocalStorage';
import { getUserInfo } from '../utils/API';


const properties={
  user:"user",
  sessoin:"session",

}

exports.saveUser=async (userData)=>{
  await localStorage.saveData(properties.user,userData);
}

exports.getUser=async ()=>{
  return await localStorage.requestData(properties.user);
}

exports.refreshUserInfo = async (userId)=>{
  let userInfo = await getUserInfo(userId);
  exports.saveUser(userInfo);

}