/**
 * Copyright (c) 2020 Raul Gomez Acuna
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
import React,{Component} from 'react';
import { Dimensions, StyleSheet, View,Text, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';

const { width: windowWidth } = Dimensions.get('window');

const CarouselItem=(item)=> { 
     return (
      <TouchableOpacity onTouch={goToArticle} style={styles.item}>
          <Card>
          <Card.Cover
            style={styles.imageStyle} 
            source={
              {
                    uri: `https://picsum.photos/200/300?grayscale`
                  }
            }
          />
        </Card>
      </TouchableOpacity>
    )
  } 

export default CarouselItem;



const styles = StyleSheet.create({
  item: {
    paddingHorizontal: 4,
    paddingVertical: 8
  },
  imageStyle: {
    width: (windowWidth - 32) / 1.5,
    height: 150,
    resizeMode: 'cover',
  },
});
