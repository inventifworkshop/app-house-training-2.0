const LocationHelper = {
  getCurrentLocation: function (options){
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => resolve(position.coords), 
      ({code, message}) =>reject(Object.assign(new Error(message), {name: "PositionError", code})),
        options);
      });
  }
}

export default LocationHelper;