import { getRequest, postRequest, putRequest, deleteRequest, getDownloadToFile } from './Requests';
import * as Linking from 'expo-linking';
let basePath = "https://services.housetrainings.com/"
let path = basePath + 'api/';

exports.getTrainingPoints = async function (location) {
  let trainingPointsUrl = 'http://trainingpoint.inventif.xyz/webservice/?Action=get_sessions&Latitude=' + location.latitude + '&Longitude=' + location.longitude + '&Range=' + 10;
  console.log("getTrainingPoints: ", trainingPointsUrl);
  let result = await getRequest(trainingPointsUrl);
  console.log("Result getTrainingPoints", result);
  return result;
}

exports.getAllTrainingPoints = async function () {
  let trainingPointsUrl = 'http://trainingpoint.inventif.xyz/webservice/?Action=get_sessions';
  console.log("getTrainingPoints: ", trainingPointsUrl);
  let result = await getRequest(trainingPointsUrl);
  console.log("Result getTrainingPoints", result);
  return result;
}


exports.sessionRequest = async function (userId, sessionId) {
  let trainingPointsUrl = 'http://trainingpoint.inventif.xyz/webservice/?Action=get_sessions';
  console.log("getTrainingPoints: ", trainingPointsUrl);
  let result = await getRequest(trainingPointsUrl);
  console.log("Result getTrainingPoints", result);
  return result;
}

exports.getTrainerInfo = async function (trainerId) {
  let trainerInfoUrl = path + 'trainer/';
  let result = await getRequest(trainerInfoUrl + trainerId);
  console.log(trainerInfoUrl + trainerId, " Info trainer ", result);
  return result ? result[0] : null;
}

exports.getNearestSessions = async function (location) {
  let nearestPointsUrl = path + 'nearsessions';
  let requestData = {
    user_latitude: location.latitude,
    user_longitude: location.longitude,
    range: 50000
  };
  console.log("Requesting nearest points");
  let response = await postRequest(nearestPointsUrl, requestData);
  return response;
}

exports.subscribeToSession = async function (userId, sessionId) {
  let subscribeUrl = path + 'subscription';
  let requestData = {
    user_id: userId,
    session_id: sessionId
  };
  let response = await postRequest(subscribeUrl, requestData);

  console.log("Subscription ", response);
  return response;
}

exports.getUserSessions = async function (userId) {
  let trainerInfoUrl = path + 'usersessions/' + userId;
  let result = await getRequest(trainerInfoUrl);
  return result ? result.sessions : null;

}

exports.getUserInfo = async function (userId) {
  let url = path + 'user/' + userId;
  let result = await getRequest(url);
  return result && result.code === 200 ? result.user : null;
}
exports.manualLogin = async function (credentials) {
  let reqData = {
    ...credentials,
    auth_method: "manual"
  }
  let url = path + "login";
  let result = await postRequest(url, reqData);
  return result;
}
exports.getWarmUpExercises = async function (userId) {
  let url = path + 'routine/warm-up-routine/' + userId;
  let result = await getRequest(url);
  return result;
}

exports.getTodaySessions = async function (userId) {
  let url = path + 'routine/dailyroutine/' + userId;
  let result = await getRequest(url);
  //TESTING 
  {/**
  let result={
    routine:{
      code: 404,
      description: "La session de el dia de hoy consiste en una serie de repeticiones particularmente utiles para fortalecer las piernas y tonificar las pantorrillas una serie de intensidad media diseñada para poder realizar en casa utilizando pocos materiales",
       exercises:[
         {
           name:"Mid squad",
           description: "1. Sit down and touch the floor with your back. 2. Lift your legs from the ground, on a 90 degrees angle 3.-Pull your feet with your hands",
           equipment: [
             {
               name:"Ball",
               optional: "Rope"
             }
           ],
           time:3000
         },
         {
           name:"Sentadillas avanzadas",
           description:"1.put your arms on the side of your body 2.Do a jump and spread lift your arms over your head, and spread your legs",
           time:1000
         },
         {
           name:"Torsion de tronco en posicion de lagartija",
           description:"1.Do a sitmovement 2.Jump without getting up 3.Repeat ",
           time:3000
         },
         {
           name:"Saltos de tijera",
           description:"1.Do a sitmovement 2.Jump without getting up 3.Repeat ",
           equipment:[
             {
               name:"rope"
             }
           ],
           time:2000
         },
         {
          name:"Saltos de tijera modificados",
          description:"1.Do a sitmovement 2.Jump without getting up 3.Repeat ",
          equipment:[
            {
              name:"rope"
            }
          ],
          time:2000
        },
       ]
     }
   }
    */}
  return result ? result : ["1", "2"];
}

exports.updateUser = async function (userId, userUpdates) {
  let url = path + 'user/' + userId;
  let result = await putRequest(url, userUpdates);
  return result ? result : null;
}

exports.deleteUser = async function (userId) {
  let url = path + 'user/' + userId;
  let result = await deleteRequest(url);
  return result ? result : null;
}

exports.manualSignIn = async function (userData) {
  // This is the structure required to sign in an user
  //   {
  //     "name": "nombre",
  //     "last_name": "primer apelido",
  //     "second_name": "segundo apellido",
  //     "phone": "telefono",
  //     "address": "direccion",
  //     "email": "vlunaramos@gmail.com",
  //     "password": "contraseña",
  //     "auth_method": "manual"
  // }
  let url = path + 'user';
  let result = await postRequest(url, userData);
  console.log("Register result is!!!! ", result)
  return result ? result : null;
}

exports.getPaymentDetails = async (userId) => {
  let url = path + 'payment/details/' + userId;
  let result = await getRequest(url);
  return result.paymentDetails;
}

exports.getQuestions = async (userId) => {
  let url = basePath + 'questionsmobile/' + userId;
  let result = await getRequest(url);
  return result;
}

exports.getPaymentHistory = async (userId) => {
  let url = path + 'payment/history/' + userId;
  let result = await getRequest(url);

  //TESTING
  {/**
  let result = {
    code: 200,
    status: "success",
    payments: [
        {
          transaction_date: "2020/07/20 20:11:38",
          amount: '150.00',
          currency: 'MXN'
        },
        {
          transaction_date: "2020/08/20 20:11:38",
          amount: '150.00',
          currency: 'MXN'
        },
        {
          transaction_date: "2020/09/20 20:11:38",
          amount: '75.00',
          currency: 'MXN'
        },
        {
          transaction_date: "2020/10/10 20:11:38",
          amount: '150.00',
          currency: 'MXN'
        },
      ]
  };
  */}

  return result ? result : null;
}

exports.getCalendarEvents = async function (userId) {
  let url = path + 'schedule/' + userId;
  let result = await getRequest(url);

  return result ? result : null;
}

exports.saveCalendarEvent = async function (data) {
  let url = path + 'schedule';
  let result = await postRequest(url, data);
  return result;
}

exports.deleteCalendarEvent = async function (eventId) {
  let url = path + 'schedule/' + eventId;
  let result = await deleteRequest(url);
  return result;
}

exports.getArticles = async function () {
  let url = path + 'article-last';
  let result = await getRequest(url);
  return result;
}

exports.getArticlesByCategory = async function (idCategory) {
  let url = path + 'article/category/' + idCategory;
  let result = await getRequest(url);
  return result;
}

exports.requestArticle = async function (articleId) {
  let url = path + 'article/' + articleId;
  let result = await getRequest(url);
  return result;
}

exports.downloadArticlePDF = async function (articleId) {
  let url = basePath + 'pdfdownload/' + articleId;
  console.log("DOWNLOADING FROM ", url);
  Linking.openURL(url);

  return true;
}