import { AsyncStorage } from 'react-native';

const storageName="@InventifTrainingPoint:";

exports.saveData=async (key,value)=>{
  const jsonValue = JSON.stringify(value);
  await _storeData(key,jsonValue);
}

exports.requestData=async (key)=>{
  const retrievedData = await _retrieveData(key);
  const objectData = JSON.parse(retrievedData)
  return objectData
}

_storeData = async (key, value) => {
  const completeKey=storageName+key;
  try {
    await AsyncStorage.setItem(completeKey, value);
  } catch (error) {
    console.log("Couldnt save the desired key: ",key, error);
  }
};

_retrieveData = async (key) => {
  const completeKey=storageName+key; 
  try {
    const value = await AsyncStorage.getItem(completeKey);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log("Couldnt find the desired key: ",key);
  }
};