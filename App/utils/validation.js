import t from 'tcomb-form-native';
import checkEmail from './API';
export default (formValidation = {
  email: t.refinement(t.String, value=>
    {
       if(/@/.test(value)){

       }else{
         return false;
       }
    }),
  password: t.refinement(t.String,(value)=>{
    return value.length>=6;
  }),
  phone: t.refinement(t.String,(value)=>{
    const reg = /^[0]?[789]\d{9}$/;
    return value.test(reg)
  })
});
