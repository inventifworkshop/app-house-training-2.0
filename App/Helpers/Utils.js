import { AsyncStorage } from 'react-native';

function removeHours(date) {
    let value = date.split(' ')[0];
    return value;
}

function isUserSubscribed(user) {

    if (user.subcription_active || user.subscription_remaining_days > 1) {
        return true;
    }

    return false
}

//Function to rename property of calendar event array
function renameProperty(array) {

    const newArray = array.map(({ id: id, from_date: start, to_date: end, title, summary }) => ({
        id,
        start,
        end,
        title,
        summary,
    }));

    console.log('rename Property: ', newArray);

    return newArray;
}

// Function to fill the calendar with dummy events
function fillWithDummyEvents() {

    let dummyEvents = [
        {
            id: '1',
            from_date: '2019-01-01 00:00:00',
            to_date: '2019-01-01 02:00:00',
            title: 'New Year Party',
            summary: 'xyz Location',
        },
        {
            id: '2',
            from_date: '2019-01-01 02:00:00',
            to_date: '2019-01-01 05:00:00',
            title: 'React Study',
            summary: 'xyz Location',
        },
        {
            id: '3',
            from_date: '2019-01-01 01:00:00',
            to_date: '2019-01-01 02:00:00',
            title: 'New Year Wishes',
            summary: 'Call to every one',
        },
        {
            id: '4',
            from_date: '2019-01-02 00:30:00',
            to_date: '2019-01-02 01:30:00',
            title: 'Parag Birthday Party',
            summary: 'Call him',
        },
        {
            id: '5',
            from_date: '2019-01-03 01:30:00',
            to_date: '2019-01-03 02:20:00',
            title: 'My Birthday Party',
            summary: 'Lets Enjoy',
        },
        {
            id: '6',
            from_date: '2019-02-04 04:10:00',
            to_date: '2019-02-04 04:40:00',
            title: 'Engg Expo 2019',
            summary: 'Expoo Vanue not confirm',
        },
        {
            id: '7',
            from_date: '2019-02-05 00:00:00',
            to_date: '2019-02-05 15:30:00',
            title: 'Course of React Native',
            summary: 'Continue course about classes',
        },
    ];

    return renameProperty(dummyEvents);
}

function setMinDate() {

    var minDate = new Date().toISOString().slice(0, 10);

    return minDate;
}

function getExercises(data) {
    let exercises = [];

    if (data) {
        for (const exercise of data.exercises) {
            exercises.push({
                name: exercise.name,
                description: exercise.description,
                image: exercise.image,
                id: exercise.id
            })

        }
        return exercises;
    }
}


//Function to generate a login and information token
function generateToken() {

    var rand = Math.random().toString(36).substr(2);

    var token = rand + rand;

    return token;
}

async function createKeySession(keyName) {
    try {
        await AsyncStorage.setItem(keyName, generateToken());
        return true;
    } catch (error) {
        return false;
    }
}

async function removeKeySession(keyName) {
    try {
        await AsyncStorage.removeItem(keyName);
        //await AsyncStorage.clear();
        return true;
    } catch (error) {
        return false;
    }
}

export {
    removeHours,
    isUserSubscribed,
    generateToken,
    fillWithDummyEvents,
    setMinDate,
    renameProperty,
    createKeySession,
    removeKeySession,
    getExercises
}