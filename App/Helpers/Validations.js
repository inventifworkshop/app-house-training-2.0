function validateString(value) {
    if (value.length > 30) {
        var xStr = value.substring(0, value.length - 20);
        return xStr + '...'
    }

    return value;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

function validateEmailConfirmation(email, emailConfirmation) {

    let response = {
        'validate': true,
        'errorMessage': ''
    };

    if (!validateEmail(email)) {
        return response = {
            'validate': false,
            'errorMessage': 'El e-mail no es valido.'
        };
    } else if (!validateEmail(emailConfirmation)) {
        return response = {
            'validate': false,
            'errorMessage': 'El e-mail de confirmación no es válido.'
        };
    } else if (email !== emailConfirmation) {
        return response = {
            'validate': false,
            'errorMessage': "Los e-mails no coinciden."
        };
    }
    return response;
}

function validatePassword(pass, passConfirm) {

    let response = {
        'validate': true,
        'errorMessage': ''
    };

    if (pass.length < 6) {
        return response = {
            'validate': false,
            'errorMessage': 'La contraseña no es válida.'
        };
    } else if (passConfirm.length < 6) {
        return response = {
            'validate': false,
            'errorMessage': 'La contraseña de confirmación no es válida.'
        };
    } else if (pass !== passConfirm) {
        return response = {
            'validate': false,
            'errorMessage': "Las contraseñas no coinciden."
        };
    }

    return response;
};

function loginValidation(email, password) {

    let response = {
        'validate': true,
        'errorMessage': ''
    };

    if (isEmpty(email) || isEmpty(password)) {
        return response = {
            'validate': false,
            'errorMessage': 'Por favor, ingrese sus credenciales.'
        };
    } else if (!validateEmail(email)) {
        return response = {
            'validate': false,
            'errorMessage': 'Por favor, ingrese un correo valido.'
        };
    }
    return response;

}

function isEmpty(text) {
    return (text === "" || text === null || onlyHasWhiteSpaces(text));
}

function onlyHasWhiteSpaces(text) {

    let regSpace = new RegExp(/^[\s]*$/);

    return (regSpace.test(text));
}

function deleteAccountConfirmation(text) {

    if (isEmpty(text) || text != 'Borrar') {
        return false;
    }

    return true;
}

function validateDate(start_date, end_date) {

    if (end_date <= start_date) {
        return true;
    } else {
        return false;
    }
}

export {
    validateString,
    validateEmail,
    validateEmailConfirmation,
    validatePassword,
    isEmpty,
    deleteAccountConfirmation,
    validateDate,
    loginValidation
}