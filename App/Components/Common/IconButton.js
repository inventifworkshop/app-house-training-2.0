
import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';

export default class IconButton extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <Image style={{ width: 40, height: 40 }} resizeMethod="scale" resizeMode="cover" source={this.props.source} ></Image>
      </TouchableOpacity>
    )
  }
}
