import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

export const CustomButton = (props) => {
    const {
        title = 'Enter',
        style = {},
        textStyle = {},
        onPress,
        onLongPress,
        delayLongPress,
        disabled,
    } = props;

    return (
        <TouchableOpacity onPress={onPress} onLongPress={onLongPress} delayLongPress={delayLongPress} style={[styles.button, style]} disabled={disabled}>
            <Text style={[styles.text, textStyle]}>{props.title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',

        shadowColor: '#2AC062',
        shadowOpacity: 0.4,
        shadowOffset: { height: 10, width: 0 },
        shadowRadius: 20,
    },
    text: {
        fontWeight: 'bold',
        fontSize: 15,
        color: '#000',
    },
});
