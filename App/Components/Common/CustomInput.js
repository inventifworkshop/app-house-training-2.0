import React from 'react';
import { StyleSheet, View, TextInput, Text } from 'react-native';

export const CustomInput = (props) => {
    const {
        title = 'Enter',
        containerStyle = {},
        textInputStyle = {},
        viewTextStyle = {},
        textStyle = {},
        onChangeText,
        value,
        placeholder = '',
        secureTextEntry = false
    } = props;

    return (

        <View style={[styles.inputContainer, containerStyle]} >
            <TextInput style={[styles.textInput, textInputStyle]} value={value} onChangeText={onChangeText} placeholder={placeholder} secureTextEntry={secureTextEntry} />
            <View style={[styles.viewStyle, viewTextStyle]}>
                <Text style={[styles.text, textStyle]}>{props.title}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        width: "100%",
        height: 64,
        flexDirection: "column",
        alignItems: 'center'
    },
    textInput: {
        marginTop: 15,
        borderColor: '#FF2D59',
        borderBottomWidth: 2,
        width: '80%',
    },
    viewStyle: {
        alignSelf: 'flex-start'
    },
    text: {
        fontWeight: 'bold'
    }
});
