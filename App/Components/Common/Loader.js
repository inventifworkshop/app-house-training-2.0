import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { containers } from '../../styles/CommonStyle';

export default Loader = (props) =>
(
  <View style={[containers.mainContainer, styles.center]} >
    <ActivityIndicator size="large" color="red" />
  </View >

)

const styles = StyleSheet.create({
  center: {
    justifyContent: "center",
    alignItems: 'center',
  }
});