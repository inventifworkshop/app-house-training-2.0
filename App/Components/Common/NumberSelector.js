
import React, { Component } from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';
import {texts} from '../../styles/CommonStyle';
export default class NumberSelector extends Component {
  constructor(props){
      super(props);
      this.state={
          value: parseInt(props.default) || 0,
          min: null,
          max: null
      }
  }


  plusAction=()=>{
      this.setState(prevState=>({
          value: prevState.value+1
      }), this.onChange);
  }
  
  subtractAction = ()=>{
      this.setState(prevState=>({
          value: prevState.value-1
      }), this.onChange);
      
  }
  onChange=()=>{
    if(this.props.onChange){
      console.log("ON CHANGE WITH ",this.state.value);
      this.props.onChange(this.state.value);
    }
  }
  render() {
    return <View style={{flexDirection:"row" , flex:1}}>
            <View style={{flex:1, backgroundColor:"#F0F0F0"}}>
                <Text>{this.state.value}</Text>
            </View>
            <View style={{flex:1, flexDirection:"row"}}>
                <View style={{flex:1, textAlign:"center"}}>  
                  <TouchableOpacity style={{flex:1}} onPress={this.subtractAction}>
                    <Text style={[texts.primaryText,{flex:1, backgroundColor:"#FF2D59",textAlign:"center"}]}>-</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity style={{flex:1}} onPress={this.plusAction}>
                    <Text  style={[texts.primaryText,{flex:1, backgroundColor:"#FF2D59",textAlign:"center"}]} >+</Text>
                  </TouchableOpacity>
                </View>
            </View>
        </View>
    
  }
}
